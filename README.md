# Bitcoin Ansible

> Project has been moved to https://www.nakamoto.codes/w3irdrobot/btc-ansible

```shell
pipenv install
pipenv shell
ansible-galaxy install -r requirements.yaml
ansible-playbook -i inventory.cfg main.yaml
```
